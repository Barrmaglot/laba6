#include <iostream>

using namespace std;

int main() {
	setlocale(LC_CTYPE, "Russian");
	const int n = 100;
	int arr[n][n];
	int b;
	cout << " Размер массива ";
	cin >> b;
	
	
	for (int i = 0; i < b; i++) {
		for (int j = 0; j < b; j++) {
			cout << " arr[" << i << " ][ " << j << " ] = ";
			cin >> arr[i][j];
		}
	}
	
	for (int i = 0; i < b; i++) {
		for (int j = 0; j < b; j++) {
			cout.width(4);
			cout << arr[i][j];
			if (j == b - 1)cout << endl;
		}
	}
	
	int sum[n];
	for (int i = 0; i < n; i++) {
		sum[i] = 0;
	}
	int nom = 0;
	
	for (int i = 0; i < b; i++) {
		for (int j = 0; j < b; j++) {
			sum[nom] += arr[i][j];
		}
		nom++;
	}
	
	for (int i = 0; i < b; i++) {
		for (int j = 0; j < b; j++) {
			sum[nom] += arr[j][i];
		}
		nom++;
	}
	
	for (int i = 0; i < b; i++) {
		for (int j = 0; j < b; j++) {
			if (i == j) {
				sum[nom] += arr[i][j];
				
			}
		}
	}
	nom++;
	
	for (int i = 0; i < b; i++) {
		sum[nom] += arr[i][b - 1 - i];
	}
	nom++;
	
	int value = sum[0], sch = 0;
	for (int i = 0; i < nom; i++) {
		if (sum[i] == value)sch++;
	}
	if (sch == nom)cout << " Это магический квадрат ";
	else cout << " Это не магический квадрат ";
	system("pause");
	return 0;
}

